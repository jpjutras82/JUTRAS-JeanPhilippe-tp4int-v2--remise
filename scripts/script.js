function scrollAppear(){
    var element_selected = document.querySelector('.element_hidden');
    var scrollPosition = element_selected.getBoundingClientRect().top;
    var screenPosition = window.innerHeight / 1.3;
    if(scrollPosition < screenPosition){
        element_selected.classList.add('element_appear')
    }
}

window.addEventListener('scroll',scrollAppear);










// Get the modal
var modal = document.getElementById("formulaire_infolettre");

// Get the button that opens the modal
var btn = document.getElementById("bouton_infolettre");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}